package com.etomun.mobile.racik.common.di.module.data

import com.etomun.mobile.racik.common.di.qualifier.data.RemoteData
import com.etomun.mobile.racik.data.remote.source.RemoteAuthSource
import com.etomun.mobile.racik.data.remote.source.RemoteUserSource
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.repository.UserRepository
import dagger.Binds
import dagger.Module

@Module
internal abstract class RemoteDataSourceModule {
    @Binds
    @RemoteData
    abstract fun authSource(remoteAuthSource: RemoteAuthSource): AuthRepository.DataSource

    @Binds
    @RemoteData
    abstract fun userSource(remoteUserSource: RemoteUserSource): UserRepository.DataSource

}