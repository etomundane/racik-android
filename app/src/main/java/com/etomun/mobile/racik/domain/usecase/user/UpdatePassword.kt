package com.etomun.mobile.racik.domain.usecase.user

import com.etomun.mobile.racik.domain.entity.param.UpdatePasswordParam
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.repository.UserRepository
import com.etomun.mobile.racik.domain.usecase.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class UpdatePassword @Inject internal constructor(
    private val userRepository: UserRepository
) :
    UseCase<Observable<GeneralResult<Boolean>>, UpdatePasswordParam> {
    override fun execute(param: UpdatePasswordParam): Observable<GeneralResult<Boolean>> {
        return userRepository.changePassword(param.oldPassword, param.newPassword)
    }

}