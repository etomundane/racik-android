package com.etomun.mobile.racik.domain.usecase.auth

import com.etomun.mobile.racik.domain.entity.param.LoginEmailParam
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserAuth
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.usecase.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class LoginByUsername @Inject internal constructor(private val authRepository: AuthRepository) :
    UseCase<Observable<GeneralResult<UserAuth>>, LoginEmailParam> {

    override fun execute(param: LoginEmailParam): Observable<GeneralResult<UserAuth>> {
        return authRepository.loginByEmail(param)
    }

}