package com.etomun.mobile.racik.presentation.main

import com.etomun.mobile.racik.common.util.rx.RxScheduler
import com.etomun.mobile.racik.common.util.state.HttpState
import com.etomun.mobile.racik.domain.repository.UserSession
import com.etomun.mobile.racik.domain.usecase.user.GetProfile
import com.etomun.mobile.racik.presentation.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class MainPresenter<V : Main.View>
@Inject internal constructor(
    session: UserSession,
    scheduler: RxScheduler,
    compositeDisposable: CompositeDisposable,
    httpEvent: PublishSubject<Pair<HttpState, String>>,
    private val getProfile: GetProfile
) : BasePresenter<V>(session, scheduler, compositeDisposable, httpEvent),
    Main.Presenter<V> {
    override fun getUserProfile() {
        enqueue(getProfile.execute(null)
            .compose(scheduler.observableIoUi())
            .doOnSubscribe { view.showMainProgressBar(true) }
            .doAfterTerminate { view.showMainProgressBar(false) }
            .subscribe(
                { view.showUserProfile(it) },
                { view.showError(it.localizedMessage) })
        )
    }
}