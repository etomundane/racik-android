package com.etomun.mobile.racik.domain.usecase.auth

import com.etomun.mobile.racik.domain.entity.param.ResetPasswordParam
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.usecase.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class ResetUserPassword @Inject internal constructor(
    private val authRepository: AuthRepository
) :
    UseCase<Observable<GeneralResult<Boolean>>, ResetPasswordParam> {
    override fun execute(param: ResetPasswordParam): Observable<GeneralResult<Boolean>> {
        return authRepository.resetPassword(param.token, param.email, param.password)
    }

}