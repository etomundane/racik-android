package com.etomun.mobile.racik.domain.entity.param

class ResetPasswordParam(val token: String, val email: String, val password: String)