package com.etomun.mobile.racik.data.remote.mapper

import com.etomun.mobile.racik.data.base.DataMapper
import com.etomun.mobile.racik.data.remote.base.DataResponse
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import javax.inject.Inject

class DataResponseMapper<T> @Inject internal constructor() :
    DataMapper<DataResponse<T>, GeneralResult<Boolean>> {
    override fun fromData(data: GeneralResult<Boolean>): DataResponse<T> {
        throw UnsupportedOperationException()
    }

    override fun toData(source: DataResponse<T>): GeneralResult<Boolean> {
        val isError = source.error
        return GeneralResult(source.code, isError, source.message.toString(), !isError)
    }
}