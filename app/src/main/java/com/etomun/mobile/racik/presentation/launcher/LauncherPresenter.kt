package com.etomun.mobile.racik.presentation.launcher

import com.etomun.mobile.racik.common.util.rx.RxScheduler
import com.etomun.mobile.racik.common.util.state.HttpState
import com.etomun.mobile.racik.domain.repository.UserSession
import com.etomun.mobile.racik.presentation.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LauncherPresenter<V : Launcher.View>
@Inject internal constructor(
    session: UserSession,
    scheduler: RxScheduler,
    compositeDisposable: CompositeDisposable,
    httpEvent: PublishSubject<Pair<HttpState, String>>
) : BasePresenter<V>(session, scheduler, compositeDisposable, httpEvent),
    Launcher.Presenter<V> {

    override fun checkAuthStatus() {
        view.switchAuthStatus(session.isLogin())
    }


}