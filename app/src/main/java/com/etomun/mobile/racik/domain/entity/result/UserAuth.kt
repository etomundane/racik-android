package com.etomun.mobile.racik.domain.entity.result

data class UserAuth(
    var id: Long,
    var email: String,
    var accessToken: String
)