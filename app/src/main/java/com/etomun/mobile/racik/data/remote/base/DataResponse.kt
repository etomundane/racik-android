package com.etomun.mobile.racik.data.remote.base

import com.google.gson.annotations.SerializedName

open class DataResponse<DATA> {
    @SerializedName("code")
    var code: Int = 0

    @SerializedName("error")
    var error = false

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: DATA? = null
}
