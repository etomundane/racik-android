package com.etomun.mobile.racik.presentation.base

import dagger.android.support.DaggerDialogFragment

abstract class BaseDialogFragment : DaggerDialogFragment(), Presentation.View