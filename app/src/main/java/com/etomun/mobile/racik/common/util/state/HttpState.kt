package com.etomun.mobile.racik.common.util.state

enum class HttpState { NO_CONNECTION, NOT_FOUND, SERVER_ERROR, UNAUTHORIZED, UNKNOWN_ERROR }