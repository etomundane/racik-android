package com.etomun.mobile.racik.data.remote.response.auth

import com.etomun.mobile.racik.data.remote.base.BaseResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class LoginResponse : BaseResponse() {
    @SerializedName("access_token")
    @Expose
    var accessToken: String = ""

    @SerializedName("email")
    @Expose
    var email: String = ""

    @SerializedName("uid")
    @Expose
    var uid: Long = 0
}