package com.etomun.mobile.racik.domain.usecase.auth

import com.etomun.mobile.racik.domain.entity.param.SignUpParam
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.usecase.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class RegisterUser @Inject internal constructor(private val authRepository: AuthRepository) :
    UseCase<Observable<GeneralResult<Boolean>>, SignUpParam> {

    override fun execute(param: SignUpParam): Observable<GeneralResult<Boolean>> {
        return authRepository.signUp(param)
    }

}