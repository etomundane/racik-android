package com.etomun.mobile.racik.presentation.main

import com.etomun.mobile.racik.domain.entity.result.UserProfile
import com.etomun.mobile.racik.presentation.base.Presentation

interface Main {
    interface View : Presentation.View {
        fun showUserProfile(userProfile: UserProfile)
    }

    interface Presenter<V : View> : Presentation.Presenter<V> {
        fun getUserProfile()
    }
}