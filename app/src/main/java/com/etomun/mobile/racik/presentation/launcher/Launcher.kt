package com.etomun.mobile.racik.presentation.launcher

import com.etomun.mobile.racik.presentation.base.Presentation

interface Launcher {
    interface View : Presentation.View {
        fun switchAuthStatus(isLogin: Boolean)
    }

    interface Presenter<V : View> : Presentation.Presenter<V> {
        fun checkAuthStatus()
    }
}