package com.etomun.mobile.racik.data.remote.api

import com.etomun.mobile.racik.common.util.constant.Api
import com.etomun.mobile.racik.data.remote.base.BaseResponse
import com.etomun.mobile.racik.data.remote.base.DataResponse
import com.etomun.mobile.racik.data.remote.response.auth.LoginResponse
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface AuthApi {

    @FormUrlEncoded
    @POST("/login")
    @Headers(Api.HEADER_NO_AUTHENTICATION + ": true")
    fun loginEmail(
        @Field("email") email: String,
        @Field("password") password: String
    ): Observable<DataResponse<LoginResponse>>

    @FormUrlEncoded
    @POST("/register")
    @Headers(Api.HEADER_NO_AUTHENTICATION + ": true")
    fun signUp(
        @Field("name") fullName: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Observable<BaseResponse>

    @FormUrlEncoded
    @POST("/password/reset")
    @Headers(Api.HEADER_NO_AUTHENTICATION + ": true")
    fun resetPassword(
        @Field("token") token: String,
        @Field("email") email: String,
        @Field("password") newPassword: String
    ): Observable<BaseResponse>

    @FormUrlEncoded
    @POST("/password/forgot")
    @Headers(Api.HEADER_NO_AUTHENTICATION + ": true")
    fun requestResetPassword(@Field("email") email: String): Observable<BaseResponse>

}