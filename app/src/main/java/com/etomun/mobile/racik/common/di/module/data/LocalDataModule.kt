package com.etomun.mobile.racik.common.di.module.data

import android.content.Context
import androidx.room.Room
import com.etomun.mobile.racik.common.di.qualifier.android.ApplicationContext
import com.etomun.mobile.racik.common.di.scope.ApplicationScope
import com.etomun.mobile.racik.data.local.database.AppDatabase
import com.etomun.mobile.racik.data.local.preference.AppPreference
import com.etomun.mobile.racik.data.local.preference.AuthPreference
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module(includes = [LocalDataSourceModule::class])
internal abstract class LocalDataModule {

    @ApplicationScope
    @Binds
    abstract fun authPref(authPreference: AuthPreference): AppPreference.AuthPref

    @Module
    companion object {
        @JvmStatic
        @ApplicationScope
        @Provides
        fun appDatabase(@ApplicationContext context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.DB_NAME)
                .fallbackToDestructiveMigration()
                .build()

    }

}