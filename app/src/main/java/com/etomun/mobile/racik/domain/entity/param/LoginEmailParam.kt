package com.etomun.mobile.racik.domain.entity.param

class LoginEmailParam(val email: String, val password: String)