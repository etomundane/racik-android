package com.etomun.mobile.racik.domain.usecase.auth

import com.etomun.mobile.racik.domain.entity.result.UserAuth
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.usecase.UseCase
import javax.inject.Inject

class GetAuthData @Inject internal constructor(private val authRepository: AuthRepository) :
    UseCase<UserAuth, Void?> {

    override fun execute(param: Void?): UserAuth {
        return authRepository.getAuth()
    }
}