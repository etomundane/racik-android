package com.etomun.mobile.racik.data.repository

import com.etomun.mobile.racik.common.di.qualifier.data.LocalData
import com.etomun.mobile.racik.common.di.qualifier.data.RemoteData
import com.etomun.mobile.racik.data.base.BaseRepository
import com.etomun.mobile.racik.domain.entity.param.LoginEmailParam
import com.etomun.mobile.racik.domain.entity.param.SignUpParam
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserAuth
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.repository.AuthRepository.DataSource
import io.reactivex.Observable
import javax.inject.Inject

class AuthRepoFactory @Inject constructor(
    @LocalData localData: DataSource,
    @RemoteData remoteData: DataSource
) :
    BaseRepository<DataSource>(localData, remoteData),
    AuthRepository {
    override fun isLogin(): Boolean {
        return localData.isLogin()
    }

    override fun loginByEmail(loginEmailParam: LoginEmailParam): Observable<GeneralResult<UserAuth>> {
        checkRemoteData()
        return remoteData!!.loginByEmail(loginEmailParam)
            .doOnNext { it.data?.let { userAuth -> localData.persistAuth(userAuth) } }
    }

    override fun signUp(signUpParam: SignUpParam): Observable<GeneralResult<Boolean>> {
        checkRemoteData()
        return remoteData!!.signUp(signUpParam)
    }

    override fun requestNewPassword(email: String): Observable<GeneralResult<Boolean>> {
        checkRemoteData()
        return remoteData!!.requestNewPassword(email)
    }

    override fun resetPassword(
        token: String, email: String, password: String
    ): Observable<GeneralResult<Boolean>> {
        checkRemoteData()
        return remoteData!!.resetPassword(token, email, password)
    }

    override fun logout(): Observable<Boolean> {
        return localData.logout()
    }

    override fun getAuth(): UserAuth {
        return localData.getAuth()
    }
}