package com.etomun.mobile.racik.data.local.preference

import android.content.Context
import com.etomun.mobile.racik.common.di.qualifier.android.ApplicationContext
import com.etomun.mobile.racik.common.di.scope.ApplicationScope
import com.etomun.mobile.racik.data.local.preference.AppPreference.AuthPref
import javax.inject.Inject

@ApplicationScope
class AuthPreference @Inject internal constructor(@ApplicationContext context: Context) :
    AppPreference(context), AuthPref {

    override fun getAuthToken(): String {
        return get(AUTH_TOKEN, "")
    }

    override fun setAuthToken(authToken: String) {
        set(AUTH_TOKEN, authToken)
    }

    override fun getUid(): Long {
        return get(UID, -1L)
    }

    override fun setUid(uid: Long) {
        set(UID, uid)
    }

    override fun getEmail(): String {
        return get(EMAIL, "")
    }

    override fun setEmail(email: String) {
        set(EMAIL, email)
    }

    companion object {
        private const val AUTH_TOKEN = "bG86UjZadYNxUhI5mdxaDRJHNrsvM0tQ"
        private const val UID = "td5KqhRMRmVg63hj4ZYDSbFD9IbsENrP"
        private const val EMAIL = "RtLO3T1atAqhFq50dkRJ8ykCHc0zBxNy"
    }
}