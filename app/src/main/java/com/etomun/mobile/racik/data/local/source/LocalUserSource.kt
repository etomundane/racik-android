package com.etomun.mobile.racik.data.local.source

import android.net.Uri
import com.etomun.mobile.racik.data.local.database.AppDatabase
import com.etomun.mobile.racik.data.local.mapper.UserProfileEntityMapper
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserProfile
import com.etomun.mobile.racik.domain.repository.UserRepository.DataSource
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class LocalUserSource @Inject internal constructor(
    private val appDatabase: AppDatabase,
    private val userProfileEntityMapper: UserProfileEntityMapper
) : DataSource {
    override fun persistUserProfile(userProfile: UserProfile): Single<Long> {
        return appDatabase.userProfileDao()
            .insertReplace(userProfileEntityMapper.fromData(userProfile))
    }

    override fun getUserProfile(): Observable<UserProfile> {
        throw UnsupportedOperationException()
    }

    override fun updateAvatar(uid: Long, uri: Uri)
            : Observable<GeneralResult<Boolean>> {
        throw UnsupportedOperationException()
    }

    override fun changePassword(
        oldPassword: String,
        newPassword: String
    ): Observable<GeneralResult<Boolean>> {
        throw UnsupportedOperationException()
    }

}