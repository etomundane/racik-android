package com.etomun.mobile.racik.domain.entity.result

data class UserProfile(
    var uid: Long,
    var fullName: String,
    var email: String,
    var avatar: String
)