package com.etomun.mobile.racik.domain.repository

import android.net.Uri
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserProfile
import io.reactivex.Observable
import io.reactivex.Single

interface UserRepository {
    fun getUserProfile(): Observable<UserProfile>
    fun updateAvatar(uid: Long, uri: Uri): Observable<GeneralResult<Boolean>>
    fun changePassword(oldPassword: String, newPassword: String): Observable<GeneralResult<Boolean>>

    interface DataSource : UserRepository {
        fun persistUserProfile(userProfile: UserProfile): Single<Long>
    }
}