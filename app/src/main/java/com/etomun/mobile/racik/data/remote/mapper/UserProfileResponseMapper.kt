package com.etomun.mobile.racik.data.remote.mapper

import com.etomun.mobile.racik.data.base.DataMapper
import com.etomun.mobile.racik.data.remote.response.user.ProfileResponse
import com.etomun.mobile.racik.domain.entity.result.UserProfile
import javax.inject.Inject

class UserProfileResponseMapper @Inject internal constructor() :
    DataMapper<ProfileResponse, UserProfile?> {
    override fun fromData(data: UserProfile?): ProfileResponse {
        throw UnsupportedOperationException()
    }

    override fun toData(source: ProfileResponse): UserProfile? {
        return UserProfile(
            source.uid,
            source.fullName,
            source.email,
            source.avatar
        )
    }
}