package com.etomun.mobile.racik.common.di.module.data

import com.etomun.mobile.racik.data.repository.AuthRepoFactory
import com.etomun.mobile.racik.data.repository.UserRepoFactory
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.repository.UserRepository
import dagger.Binds
import dagger.Module

@Module(includes = [LocalDataModule::class, RemoteDataModule::class])
internal abstract class DataModule {
    @Binds
    abstract fun authRepository(authRepoFactory: AuthRepoFactory): AuthRepository

    @Binds
    abstract fun userRepository(userRepoFactory: UserRepoFactory): UserRepository

}