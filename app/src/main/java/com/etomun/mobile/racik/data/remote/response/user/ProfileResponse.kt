package com.etomun.mobile.racik.data.remote.response.user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProfileResponse {
    @SerializedName("uid")
    @Expose
    var uid: Long = 0

    @SerializedName("full_name")
    @Expose
    var fullName: String = ""

    @SerializedName("email")
    @Expose
    var email: String = ""

    @SerializedName("avatar")
    @Expose
    var avatar: String = ""
}