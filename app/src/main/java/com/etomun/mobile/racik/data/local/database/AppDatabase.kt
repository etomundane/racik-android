package com.etomun.mobile.racik.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.etomun.mobile.racik.data.local.database.user.UserProfileDao
import com.etomun.mobile.racik.data.local.database.user.UserProfileEntity

@Database(
    version = AppDatabase.DB_VERSION,
    exportSchema = false,
    entities = [
        UserProfileEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userProfileDao(): UserProfileDao

    companion object {
        const val DB_NAME = "DGvC3freBqAepnVI9WX38NkIeVAY7TyP"
        const val DB_VERSION = 1
    }

}