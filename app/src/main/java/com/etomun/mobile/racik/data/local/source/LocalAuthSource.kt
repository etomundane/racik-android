package com.etomun.mobile.racik.data.local.source

import com.etomun.mobile.racik.data.local.preference.AppPreference.AuthPref
import com.etomun.mobile.racik.domain.entity.param.LoginEmailParam
import com.etomun.mobile.racik.domain.entity.param.SignUpParam
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserAuth
import com.etomun.mobile.racik.domain.repository.AuthRepository.DataSource
import io.reactivex.Observable
import javax.inject.Inject

class LocalAuthSource @Inject internal constructor(private val authPref: AuthPref) : DataSource {
    override fun persistAuth(auth: UserAuth) {
        authPref.setAuthToken(auth.accessToken)
        authPref.setUid(auth.id)
        authPref.setEmail(auth.email)
    }

    override fun isLogin(): Boolean {
        return authPref.getAuthToken().isNotEmpty() && authPref.getUid() > 0
    }

    override fun loginByEmail(loginEmailParam: LoginEmailParam): Observable<GeneralResult<UserAuth>> {
        throw UnsupportedOperationException()
    }

    override fun signUp(signUpParam: SignUpParam): Observable<GeneralResult<Boolean>> {
        throw UnsupportedOperationException()
    }

    override fun requestNewPassword(email: String): Observable<GeneralResult<Boolean>> {
        throw UnsupportedOperationException()
    }

    override fun resetPassword(
        token: String, email: String, password: String
    ): Observable<GeneralResult<Boolean>> {
        throw UnsupportedOperationException()
    }

    override fun logout(): Observable<Boolean> {
        authPref.setAuthToken("")
        authPref.setUid(-1L)
        authPref.setEmail("")
        return Observable.just(true)
    }

    override fun getAuth(): UserAuth {
        return UserAuth(
            authPref.getUid(),
            authPref.getEmail(),
            authPref.getAuthToken()
        )
    }

}