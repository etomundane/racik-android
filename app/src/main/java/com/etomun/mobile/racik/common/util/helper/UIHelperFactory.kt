package com.etomun.mobile.racik.common.util.helper

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.graphics.Point
import android.net.Uri
import android.view.View
import com.etomun.mobile.racik.common.di.qualifier.android.ApplicationContext
import java.io.ByteArrayOutputStream
import java.io.IOException
import javax.inject.Inject
import kotlin.math.*

class UIHelperFactory @Inject constructor(@ApplicationContext private val context: Context) :
    UIHelper {
    override fun getCenterPointOfView(view: View?): Point? {
        val cx = (view!!.left + view.right) / 2
        val cy = (view.top + view.bottom) / 2
        return Point(cx, cy)
    }

    override fun getCircleRadiusOfView(view: View?): Float {
        // get the center for the clipping circle
        // get the center for the clipping circle
        val cx = (view!!.left + view.right) / 2
        val cy = (view.top + view.bottom) / 2

        // get the final radius for the clipping circle
        // get the final radius for the clipping circle
        val dx = max(cx, view.width - cx)
        val dy = max(cy, view.height - cy)
        return hypot(dx.toDouble(), dy.toDouble()).toFloat()
    }

    override fun getResId(resName: String?, c: Class<*>): Int {
        return try {
            val idField = c.getDeclaredField(resName!!)
            idField.getInt(idField)
        } catch (e: Exception) {
            e.printStackTrace()
            -1
        }
    }

    override fun getRawId(resName: String): Int {
        val fileName = resName.substring(6)
        return context.resources.getIdentifier(fileName, "raw", context.packageName)
    }

    @Throws(IOException::class)
    override fun blur(
        uri: Uri,
        scale: Float,
        radius: Float
    ): ByteArrayOutputStream? {
        val bitmapInput =
            ImageDecoder.decodeBitmap(ImageDecoder.createSource(context.contentResolver, uri))
        return blur(bitmapInput, scale, radius)
    }

    override fun blur(
        resId: Int,
        scale: Float,
        radius: Float
    ): ByteArrayOutputStream? {
        val options = BitmapFactory.Options()
        options.inSampleSize = 8
        val sentBitmap =
            BitmapFactory.decodeResource(context.resources, resId, options)
        return blur(sentBitmap, scale, radius)
    }

    /**
     * scale: Float number 0 - 1
     * radius: Float number > 1
     */
    private fun blur(
        sentBitmap: Bitmap,
        scale: Float,
        radius: Float
    ): ByteArrayOutputStream? {
        var bitmap1 = sentBitmap
        val width = (bitmap1.width * scale).roundToInt()
        val height = (bitmap1.height * scale).roundToInt()
        bitmap1 = Bitmap.createScaledBitmap(bitmap1, width, height, false)
        val bitmap = bitmap1.copy(bitmap1.config, true)
        if (radius < 1) {
            return null
        }
        val w = bitmap.width
        val h = bitmap.height
        val pix = IntArray(w * h)
        bitmap.getPixels(pix, 0, w, 0, 0, w, h)
        val wm = w - 1
        val hm = h - 1
        val wh = w * h
        val div = (radius + radius + 1).toInt()
        val r = IntArray(wh)
        val g = IntArray(wh)
        val b = IntArray(wh)
        var rsum: Int
        var gsum: Int
        var bsum: Int
        var x: Int
        var y: Int
        var i: Int
        var p: Int
        var yp: Int
        var yi: Int
        var yw: Int
        val vMin = IntArray(max(w, h))
        var divsum = div + 1 shr 1
        divsum *= divsum
        val dv = IntArray(256 * divsum)
        i = 0
        while (i < 256 * divsum) {
            dv[i] = i / divsum
            i++
        }
        yi = 0
        yw = yi
        val stack =
            Array(div) { IntArray(3) }
        var stackpointer: Int
        var stackstart: Int
        var sir: IntArray
        var rbs: Int
        val r1 = (radius + 1).toInt()
        var routsum: Int
        var goutsum: Int
        var boutsum: Int
        var rinsum: Int
        var ginsum: Int
        var binsum: Int
        y = 0
        while (y < h) {
            bsum = 0
            gsum = bsum
            rsum = gsum
            boutsum = rsum
            goutsum = boutsum
            routsum = goutsum
            binsum = routsum
            ginsum = binsum
            rinsum = ginsum
            i = (-radius).toInt()
            while (i <= radius) {
                p = pix[yi + min(wm, max(i, 0))]
                sir = stack[(i + radius).toInt()]
                sir[0] = p and 0xff0000 shr 16
                sir[1] = p and 0x00ff00 shr 8
                sir[2] = p and 0x0000ff
                rbs = r1 - abs(i)
                rsum += sir[0] * rbs
                gsum += sir[1] * rbs
                bsum += sir[2] * rbs
                if (i > 0) {
                    rinsum += sir[0]
                    ginsum += sir[1]
                    binsum += sir[2]
                } else {
                    routsum += sir[0]
                    goutsum += sir[1]
                    boutsum += sir[2]
                }
                i++
            }
            stackpointer = radius.toInt()
            x = 0
            while (x < w) {
                r[yi] = dv[rsum]
                g[yi] = dv[gsum]
                b[yi] = dv[bsum]
                rsum -= routsum
                gsum -= goutsum
                bsum -= boutsum
                stackstart = (stackpointer - radius + div).toInt()
                sir = stack[stackstart % div]
                routsum -= sir[0]
                goutsum -= sir[1]
                boutsum -= sir[2]
                if (y == 0) {
                    vMin[x] = min(x + radius + 1, wm.toFloat()).toInt()
                }
                p = pix[yw + vMin[x]]
                sir[0] = p and 0xff0000 shr 16
                sir[1] = p and 0x00ff00 shr 8
                sir[2] = p and 0x0000ff
                rinsum += sir[0]
                ginsum += sir[1]
                binsum += sir[2]
                rsum += rinsum
                gsum += ginsum
                bsum += binsum
                stackpointer = (stackpointer + 1) % div
                sir = stack[stackpointer % div]
                routsum += sir[0]
                goutsum += sir[1]
                boutsum += sir[2]
                rinsum -= sir[0]
                ginsum -= sir[1]
                binsum -= sir[2]
                yi++
                x++
            }
            yw += w
            y++
        }
        x = 0
        while (x < w) {
            bsum = 0
            gsum = bsum
            rsum = gsum
            boutsum = rsum
            goutsum = boutsum
            routsum = goutsum
            binsum = routsum
            ginsum = binsum
            rinsum = ginsum
            yp = (-radius * w).toInt()
            i = (-radius).toInt()
            while (i <= radius) {
                yi = max(0, yp) + x
                sir = stack[(i + radius).toInt()]
                sir[0] = r[yi]
                sir[1] = g[yi]
                sir[2] = b[yi]
                rbs = r1 - abs(i)
                rsum += r[yi] * rbs
                gsum += g[yi] * rbs
                bsum += b[yi] * rbs
                if (i > 0) {
                    rinsum += sir[0]
                    ginsum += sir[1]
                    binsum += sir[2]
                } else {
                    routsum += sir[0]
                    goutsum += sir[1]
                    boutsum += sir[2]
                }
                if (i < hm) {
                    yp += w
                }
                i++
            }
            yi = x
            stackpointer = radius.toInt()
            y = 0
            while (y < h) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] =
                    -0x1000000 and pix[yi] or (dv[rsum] shl 16) or (dv[gsum] shl 8) or dv[bsum]
                rsum -= routsum
                gsum -= goutsum
                bsum -= boutsum
                stackstart = (stackpointer - radius + div).toInt()
                sir = stack[stackstart % div]
                routsum -= sir[0]
                goutsum -= sir[1]
                boutsum -= sir[2]
                if (x == 0) {
                    vMin[y] = (y + r1).coerceAtMost(hm) * w
                }
                p = x + vMin[y]
                sir[0] = r[p]
                sir[1] = g[p]
                sir[2] = b[p]
                rinsum += sir[0]
                ginsum += sir[1]
                binsum += sir[2]
                rsum += rinsum
                gsum += ginsum
                bsum += binsum
                stackpointer = (stackpointer + 1) % div
                sir = stack[stackpointer]
                routsum += sir[0]
                goutsum += sir[1]
                boutsum += sir[2]
                rinsum -= sir[0]
                ginsum -= sir[1]
                binsum -= sir[2]
                yi += w
                y++
            }
            x++
        }
        bitmap.setPixels(pix, 0, w, 0, 0, w, h)
        //
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream
    }
}