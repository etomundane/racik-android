package com.etomun.mobile.racik.data.local.mapper

import com.etomun.mobile.racik.data.base.DataMapper
import com.etomun.mobile.racik.data.local.database.user.UserProfileEntity
import com.etomun.mobile.racik.domain.entity.result.UserProfile
import javax.inject.Inject

class UserProfileEntityMapper @Inject internal constructor() :
    DataMapper<UserProfileEntity, UserProfile> {

    override fun fromData(data: UserProfile): UserProfileEntity {
        return UserProfileEntity(
            data.uid,
            data.fullName,
            data.email,
            data.avatar
        )
    }

    override fun toData(source: UserProfileEntity): UserProfile {
        return UserProfile(
            source.uid,
            source.fullName,
            source.email,
            source.avatar
        )
    }
}