package com.etomun.mobile.racik.domain.repository

import io.reactivex.Observable

interface UserSession {
    fun isLogin(): Boolean
    fun doLogout(): Observable<Boolean>
}