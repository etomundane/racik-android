package com.etomun.mobile.racik.common.util.helper

import android.graphics.Point
import android.net.Uri
import android.view.View
import java.io.ByteArrayOutputStream

interface UIHelper {
    fun getCenterPointOfView(view: View?): Point?
    fun getCircleRadiusOfView(view: View?): Float
    fun getResId(resName: String?, c: Class<*>): Int
    fun getRawId(resName: String): Int
    fun blur(uri: Uri, scale: Float, radius: Float): ByteArrayOutputStream?
    fun blur(resId: Int, scale: Float, radius: Float): ByteArrayOutputStream?
}