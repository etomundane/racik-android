package com.etomun.mobile.racik.domain.entity.result

data class GeneralResult<T>(
    val actionCode: Int,
    val isError: Boolean,
    val message: String,
    val data: T?
)