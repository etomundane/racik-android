package com.etomun.mobile.racik.common.di.module.data

import com.etomun.mobile.racik.common.di.qualifier.data.LocalData
import com.etomun.mobile.racik.data.local.source.LocalAuthSource
import com.etomun.mobile.racik.data.local.source.LocalUserSource
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.repository.UserRepository
import dagger.Binds
import dagger.Module

@Module
internal abstract class LocalDataSourceModule {
    @Binds
    @LocalData
    abstract fun authSource(localAuthSource: LocalAuthSource): AuthRepository.DataSource

    @Binds
    @LocalData
    abstract fun userSource(localUserSource: LocalUserSource): UserRepository.DataSource

}