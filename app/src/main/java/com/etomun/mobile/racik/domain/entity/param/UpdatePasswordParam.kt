package com.etomun.mobile.racik.domain.entity.param

class UpdatePasswordParam(val oldPassword: String, val newPassword: String)