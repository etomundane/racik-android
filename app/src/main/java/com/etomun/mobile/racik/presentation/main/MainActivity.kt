package com.etomun.mobile.racik.presentation.main

import android.os.Bundle
import com.etomun.mobile.racik.R
import com.etomun.mobile.racik.domain.entity.result.UserProfile
import com.etomun.mobile.racik.presentation.base.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity(), Main.View {

    @Inject
    lateinit var presenter: Main.Presenter<Main.View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.attachView(this, this.lifecycle)
    }

    override fun showUserProfile(userProfile: UserProfile) {

    }

    override fun onPresenterAttached() {

    }

    override fun onConnectionChanged(isConnected: Boolean) {

    }

    override fun showMainProgressBar(show: Boolean) {

    }

    override fun showError(message: String?) {

    }

    override fun onLoggedOut(hasLoggedOut: Boolean) {

    }
}
