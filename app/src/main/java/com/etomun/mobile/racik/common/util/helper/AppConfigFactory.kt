package com.etomun.mobile.racik.common.util.helper

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import com.etomun.mobile.racik.common.di.qualifier.android.ApplicationContext
import com.etomun.mobile.racik.common.util.state.DayGreetings
import java.util.*
import javax.inject.Inject

class AppConfigFactory @Inject internal constructor(@ApplicationContext private val context: Context) :
    AppConfig {

    override fun getNotificationChannel(channelName: String?, importance: Int): String {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return ""
        }
        val chan = NotificationChannel(channelName, channelName, importance)
        chan.lightColor = Color.WHITE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        chan.enableVibration(true)
        chan.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
        val service = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (service.getNotificationChannel(channelName) == null) {
            service.createNotificationChannel(chan)
        }
        return channelName!!
    }

    override fun getDayGreetings(): DayGreetings {
        val c = Calendar.getInstance()
        return when (c.get(Calendar.HOUR_OF_DAY)) {
            in 5..11 -> DayGreetings.MORNING
            in 12..16 -> DayGreetings.AFTERNOON
            in 17..21 -> DayGreetings.EVENING
            in 22..23 -> DayGreetings.NIGHT
            else -> {
                DayGreetings.OTHER
            }
        }
    }

}