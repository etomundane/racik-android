package com.etomun.mobile.racik.data.remote.mapper

import com.etomun.mobile.racik.data.base.DataMapper
import com.etomun.mobile.racik.data.remote.response.auth.LoginResponse
import com.etomun.mobile.racik.domain.entity.result.UserAuth
import javax.inject.Inject

class LoginResponseMapper @Inject internal constructor() : DataMapper<LoginResponse, UserAuth?> {
    override fun fromData(data: UserAuth?): LoginResponse {
        throw UnsupportedOperationException()
    }

    override fun toData(source: LoginResponse): UserAuth? {
        return UserAuth(
            source.uid,
            source.email,
            source.accessToken
        )
    }
}