package com.etomun.mobile.racik.common.di.qualifier.config

import javax.inject.Qualifier

@Qualifier
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class DatabaseName