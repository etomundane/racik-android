package com.etomun.mobile.racik.data.remote.api

import com.etomun.mobile.racik.data.remote.base.DataResponse
import com.etomun.mobile.racik.data.remote.response.user.ProfileResponse
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface UserApi {
    @GET("/user/profile")
    fun getProfile(): Observable<DataResponse<ProfileResponse>>

    @FormUrlEncoded
    @POST("/user/password")
    fun changePassword(
        @Field("old_password") oldPassword: String,
        @Field("password") newPassword: String
    ): Observable<DataResponse<Void?>>

    @Multipart
    @POST("/user/avatar")
    fun changeAvatar(
        @Part("user_id") uid: RequestBody,
        @Part avatar: MultipartBody.Part
    ): Observable<DataResponse<Void?>>
}