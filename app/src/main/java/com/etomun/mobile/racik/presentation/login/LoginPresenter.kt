package com.etomun.mobile.racik.presentation.login

import android.text.Editable
import com.etomun.mobile.racik.common.util.helper.AppValidator
import com.etomun.mobile.racik.common.util.rx.RxScheduler
import com.etomun.mobile.racik.common.util.state.HttpState
import com.etomun.mobile.racik.domain.entity.param.LoginEmailParam
import com.etomun.mobile.racik.domain.usecase.auth.LoginByUsername
import com.etomun.mobile.racik.domain.repository.UserSession
import com.etomun.mobile.racik.presentation.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LoginPresenter<V : Login.View>
@Inject internal constructor(
    session: UserSession,
    scheduler: RxScheduler,
    compositeDisposable: CompositeDisposable,
    httpEvent: PublishSubject<Pair<HttpState, String>>,
    private val appValidator: AppValidator,
    private val loginByUsername: LoginByUsername
) : BasePresenter<V>(session, scheduler, compositeDisposable, httpEvent),
    Login.Presenter<V> {
    private var isEmailValid = false
    private var isPasswordValid = false

    override fun loginUsername(username: String, plainPassword: String) {
        enqueue(loginByUsername.execute(LoginEmailParam(username, plainPassword))
            .compose(scheduler.observableIoUi())
            .doOnSubscribe { view.showMainProgressBar(true) }
            .doAfterTerminate { view.showMainProgressBar(false) }
            .subscribe(
                {
                    if (it.isError) {
                        view.showError(it.message)
                    } else {
                        view.onLoginSuccess()
                    }
                },
                { view.showError(it.localizedMessage) })
        )
    }

    override fun onEmailChanged(it: Editable?) {
        isEmailValid = appValidator.isEmailValid(it.toString())
        val length: Int = it?.length ?: 0
        val showError = length > 2 && !isEmailValid
        view.onEmailValidated(!showError)
        validateForms()
    }

    override fun onPasswordChanged(it: Editable?) {
        isPasswordValid = appValidator.isPasswordValid(it.toString())
        val length: Int = it?.length ?: 0
        val showError = length > 2 && !isPasswordValid
        view.onPasswordValidated(!showError)
        validateForms()
    }

    private fun validateForms() {
        view.onFormsValidated(isEmailValid && isPasswordValid)
    }
}