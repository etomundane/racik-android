package com.etomun.mobile.racik.data.remote.base

import com.google.gson.annotations.SerializedName

open class BaseResponse {
    @SerializedName("error")
    var error: Boolean = false

    @SerializedName("message")
    var message: String = ""
}