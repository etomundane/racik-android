package com.etomun.mobile.racik.common.di.module.receiver;

import com.etomun.mobile.racik.common.util.framework.ConnectivityReceiver;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BroadcastReceiverBuilder {

    @ContributesAndroidInjector
    abstract ConnectivityReceiver connectivityReceiver();

}
