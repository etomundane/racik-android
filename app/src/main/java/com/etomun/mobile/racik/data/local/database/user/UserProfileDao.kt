package com.etomun.mobile.racik.data.local.database.user

import androidx.room.Dao
import androidx.room.Query
import com.etomun.mobile.racik.data.local.base.BaseDao
import io.reactivex.Single

@Dao
interface UserProfileDao : BaseDao<UserProfileEntity> {
    @Query("select * from  ${UserProfileEntity.TABLE_NAME} where ${UserProfileEntity.C_UID} = :stallId")
    fun getProducts(stallId: Long): Single<List<UserProfileEntity>>

    @Query("delete from  ${UserProfileEntity.TABLE_NAME} where ${UserProfileEntity.C_UID} = :stallId")
    fun deleteProducts(stallId: Long): Single<Int>

    @Query("delete from  ${UserProfileEntity.TABLE_NAME}")
    fun clearTable(): Int
}