package com.etomun.mobile.racik.common.di.component

import android.app.Application
import com.etomun.mobile.racik.Racik
import com.etomun.mobile.racik.common.di.module.AppModule
import com.etomun.mobile.racik.common.di.scope.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.worker.AndroidWorkerInjectionModule

@ApplicationScope
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AndroidWorkerInjectionModule::class,
        AppModule::class
    ]
)
interface AppComponent : AndroidInjector<Racik> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}