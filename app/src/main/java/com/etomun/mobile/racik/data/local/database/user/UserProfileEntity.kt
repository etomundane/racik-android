package com.etomun.mobile.racik.data.local.database.user

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = UserProfileEntity.TABLE_NAME,
    indices = [Index(value = [UserProfileEntity.C_ID, UserProfileEntity.C_UID], unique = true)]
)
class UserProfileEntity(
    @ColumnInfo(name = C_UID)
    var uid: Long,
    var fullName: String,
    var email: String,
    var avatar: String
) {
    @ColumnInfo(name = C_ID)
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    companion object {
        const val TABLE_NAME = "zieaBYUbIILjYwc5Y1IB8tvckvSj3YnF"
        const val C_ID = "gP6snvC0RT2lNcCDGHuUqxJ5wdpNEsu7"
        const val C_UID = "YWxA9qJu03prITsuyEaEnH8RM6V0Vc1X"
    }
}