package com.etomun.mobile.racik.data.remote.mapper

import com.etomun.mobile.racik.data.base.DataMapper
import com.etomun.mobile.racik.data.remote.base.BaseResponse
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import javax.inject.Inject

class BaseResponseMapper @Inject internal constructor() :
    DataMapper<BaseResponse, GeneralResult<Boolean>> {
    override fun fromData(data: GeneralResult<Boolean>): BaseResponse {
        throw UnsupportedOperationException()
    }

    override fun toData(source: BaseResponse): GeneralResult<Boolean> {
        val isError = source.error
        return GeneralResult(source.hashCode(), isError, source.message, !isError)
    }
}