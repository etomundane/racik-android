package com.etomun.mobile.racik.common.util.state

enum class DayGreetings { MORNING, AFTERNOON, EVENING, NIGHT, OTHER }