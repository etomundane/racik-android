package com.etomun.mobile.racik.common.util.http

import com.etomun.mobile.racik.common.util.constant.Api
import com.etomun.mobile.racik.data.local.preference.AppPreference
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class AuthRequestInterceptor @Inject constructor(private val authPref: AppPreference.AuthPref) :
    Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()
//            .addHeader(Api.API_KEY, BuildConfig.API_KEY)
        if (request.header(Api.HEADER_NO_AUTHENTICATION) == null) {
            val bearerToken = "Bearer " + authPref.getAuthToken()
            requestBuilder.addHeader(Api.HEADER_AUTHORIZATION, bearerToken)
        }

        return chain.proceed(requestBuilder.build())
    }

}