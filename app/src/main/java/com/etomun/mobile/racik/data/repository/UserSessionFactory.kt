package com.etomun.mobile.racik.data.repository

import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.repository.UserSession
import io.reactivex.Observable
import javax.inject.Inject

class UserSessionFactory @Inject internal constructor(private val authRepository: AuthRepository) :
    UserSession {

    override fun isLogin(): Boolean = authRepository.isLogin()

    override fun doLogout(): Observable<Boolean> = authRepository.logout()
}