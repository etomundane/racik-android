package com.etomun.mobile.racik.domain.repository

import com.etomun.mobile.racik.domain.entity.param.LoginEmailParam
import com.etomun.mobile.racik.domain.entity.param.SignUpParam
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserAuth
import io.reactivex.Observable

interface AuthRepository {
    fun isLogin(): Boolean
    fun loginByEmail(loginEmailParam: LoginEmailParam): Observable<GeneralResult<UserAuth>>
    fun signUp(signUpParam: SignUpParam): Observable<GeneralResult<Boolean>>
    fun requestNewPassword(email: String): Observable<GeneralResult<Boolean>>
    fun resetPassword(
        token: String,
        email: String,
        password: String
    ): Observable<GeneralResult<Boolean>>

    fun logout(): Observable<Boolean>
    fun getAuth(): UserAuth

    interface DataSource :
        AuthRepository {
        fun persistAuth(auth: UserAuth)
    }
}