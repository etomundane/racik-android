package com.etomun.mobile.racik.common.util.constant

object Api {
    const val HEADER_NO_AUTHENTICATION = "No-Authentication"
    const val API_KEY = "Api-token"
    const val HEADER_AUTHORIZATION = "Authorization"
}