package com.etomun.mobile.racik.common.di.module.util

import com.etomun.mobile.racik.common.di.scope.ApplicationScope
import com.etomun.mobile.racik.common.util.framework.DeviceUtil
import com.etomun.mobile.racik.common.util.framework.DeviceUtilFactory
import com.etomun.mobile.racik.common.util.helper.*
import com.etomun.mobile.racik.common.util.rx.RxScheduler
import com.etomun.mobile.racik.common.util.rx.RxSchedulerProvider
import com.etomun.mobile.racik.common.util.state.HttpState
import com.etomun.mobile.racik.domain.repository.UserSession
import com.etomun.mobile.racik.data.repository.UserSessionFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

@Module
internal abstract class UtilModule {
    @ApplicationScope
    @Binds
    abstract fun userSession(userSessionFactory: UserSessionFactory): UserSession

    @ApplicationScope
    @Binds
    abstract fun dataConverter(dataConverterFactory: DataConverterFactory): DataConverter

    @ApplicationScope
    @Binds
    abstract fun appValidator(appValidatorFactory: AppValidatorFactory): AppValidator

    @ApplicationScope
    @Binds
    abstract fun rxScheduler(rxSchedulerProvider: RxSchedulerProvider): RxScheduler

    @ApplicationScope
    @Binds
    abstract fun deviceUtil(deviceUtilFactory: DeviceUtilFactory): DeviceUtil

    @ApplicationScope
    @Binds
    abstract fun appConfig(appConfigFactory: AppConfigFactory): AppConfig

    @ApplicationScope
    @Binds
    abstract fun uiHelper(uiHelperFactory: UIHelperFactory): UIHelper

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

        @JvmStatic
        @ApplicationScope
        @Provides
        fun httpEvent(): PublishSubject<Pair<HttpState, String>> = PublishSubject.create()
    }
}