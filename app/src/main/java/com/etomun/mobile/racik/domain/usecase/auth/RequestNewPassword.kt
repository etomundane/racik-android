package com.etomun.mobile.racik.domain.usecase.auth

import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.usecase.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class RequestNewPassword @Inject internal constructor(private val authRepository: AuthRepository) :
    UseCase<Observable<GeneralResult<Boolean>>, String> {

    override fun execute(param: String): Observable<GeneralResult<Boolean>> {
        return authRepository.requestNewPassword(param)
    }

}