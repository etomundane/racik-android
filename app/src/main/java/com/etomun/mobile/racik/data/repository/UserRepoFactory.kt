package com.etomun.mobile.racik.data.repository

import android.net.Uri
import com.etomun.mobile.racik.common.di.qualifier.data.LocalData
import com.etomun.mobile.racik.common.di.qualifier.data.RemoteData
import com.etomun.mobile.racik.data.base.BaseRepository
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserProfile
import com.etomun.mobile.racik.domain.repository.UserRepository
import com.etomun.mobile.racik.domain.repository.UserRepository.DataSource
import io.reactivex.Observable
import javax.inject.Inject

class UserRepoFactory @Inject constructor(
    @LocalData localData: DataSource,
    @RemoteData remoteData: DataSource
) :
    BaseRepository<DataSource>(localData, remoteData),
    UserRepository {
    override fun getUserProfile(): Observable<UserProfile> {
        checkRemoteData()
        return Observable.mergeDelayError(
            localData.getUserProfile(),
            remoteData!!.getUserProfile().flatMap { profile ->
                localData.persistUserProfile(profile)
                    .flatMapObservable {
                        Observable.just(profile)
                    }
            }
        )
    }

    override fun updateAvatar(uid: Long, uri: Uri): Observable<GeneralResult<Boolean>> {
        checkRemoteData()
        return remoteData!!.updateAvatar(uid, uri)
    }

    override fun changePassword(
        oldPassword: String,
        newPassword: String
    ): Observable<GeneralResult<Boolean>> {
        checkRemoteData()
        return remoteData!!.changePassword(oldPassword, newPassword)
    }
}