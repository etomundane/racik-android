package com.etomun.mobile.racik.domain.entity.param

class SignUpParam(val fullName: String, val email: String, val password: String)