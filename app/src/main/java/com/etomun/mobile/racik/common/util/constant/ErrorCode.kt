package com.etomun.mobile.racik.common.util.constant

object ErrorCode {
    const val INCORRECT_OLD_PASSWORD = 22
    const val EXPIRED_AUTH_TOKEN = 23
}