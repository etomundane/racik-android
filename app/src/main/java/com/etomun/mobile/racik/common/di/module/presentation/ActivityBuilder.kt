package com.etomun.mobile.racik.common.di.module.presentation

import com.etomun.mobile.racik.presentation.launcher.Launcher
import com.etomun.mobile.racik.presentation.launcher.LauncherActivity
import com.etomun.mobile.racik.presentation.launcher.LauncherPresenter
import com.etomun.mobile.racik.presentation.login.Login
import com.etomun.mobile.racik.presentation.login.LoginActivity
import com.etomun.mobile.racik.presentation.login.LoginPresenter
import com.etomun.mobile.racik.presentation.main.Main
import com.etomun.mobile.racik.presentation.main.MainActivity
import com.etomun.mobile.racik.presentation.main.MainPresenter
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector
    abstract fun launcherActivity(): LauncherActivity

    @Binds
    abstract fun launcherPresenter(launcherPresenter: LauncherPresenter<Launcher.View>): Launcher.Presenter<Launcher.View>

    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @Binds
    abstract fun mainPresenter(MainPresenter: MainPresenter<Main.View>): Main.Presenter<Main.View>

    @ContributesAndroidInjector
    abstract fun loginActivity(): LoginActivity

    @Binds
    abstract fun loginPresenter(LoginPresenter: LoginPresenter<Login.View>): Login.Presenter<Login.View>
}