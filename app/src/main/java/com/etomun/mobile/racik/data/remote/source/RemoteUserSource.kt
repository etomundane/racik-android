package com.etomun.mobile.racik.data.remote.source

import android.net.Uri
import com.etomun.mobile.racik.data.remote.api.UserApi
import com.etomun.mobile.racik.data.remote.mapper.DataResponseMapper
import com.etomun.mobile.racik.data.remote.mapper.UserProfileResponseMapper
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserProfile
import com.etomun.mobile.racik.domain.repository.UserRepository.DataSource
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject


class RemoteUserSource @Inject internal constructor(
    private val userApi: UserApi,
    private val userProfileResponseMapper: UserProfileResponseMapper,
    private val dataResponseMapper: DataResponseMapper<Void?>
) : DataSource {
    override fun persistUserProfile(userProfile: UserProfile): Single<Long> {
        throw UnsupportedOperationException()
    }

    override fun getUserProfile(): Observable<UserProfile> {
        return userApi.getProfile()
            .filter { !it.error }
            .map { it.data?.let { profile -> userProfileResponseMapper.toData(profile) } }
    }

    override fun updateAvatar(uid: Long, uri: Uri)
            : Observable<GeneralResult<Boolean>> {

        val id = uid.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val avatarFile = File(uri.path.toString())
        val avatarBody = avatarFile.asRequestBody("image/*".toMediaTypeOrNull())
        val avatar = MultipartBody.Part.createFormData("image", avatarFile.name, avatarBody)
        return userApi.changeAvatar(id, avatar)
            .map { dataResponseMapper.toData(it) }
    }

    override fun changePassword(
        oldPassword: String,
        newPassword: String
    ): Observable<GeneralResult<Boolean>> {
        return userApi.changePassword(oldPassword, newPassword)
            .map { dataResponseMapper.toData(it) }
    }
}