package com.etomun.mobile.racik.presentation.login

import android.text.Editable
import com.etomun.mobile.racik.presentation.base.Presentation

interface Login {
    interface View : Presentation.View {
        fun onLoginSuccess()
        fun onEmailValidated(isValid: Boolean)
        fun onPasswordValidated(isValid: Boolean)
        fun onFormsValidated(isValid: Boolean)
    }

    interface Presenter<V : View> : Presentation.Presenter<V> {
        fun loginUsername(username: String, plainPassword: String)
        fun onEmailChanged(it: Editable?)
        fun onPasswordChanged(it: Editable?)
    }
}