package com.etomun.mobile.racik.presentation.login

import android.content.Intent
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import com.etomun.mobile.racik.R
import com.etomun.mobile.racik.presentation.base.BaseChildActivity
import com.etomun.mobile.racik.presentation.main.MainActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginActivity : BaseChildActivity(), Login.View {

    @Inject
    lateinit var presenter: Login.Presenter<Login.View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        presenter.attachView(this, this.lifecycle)
    }

    override fun onLoginSuccess() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onEmailValidated(isValid: Boolean) {
        form_email.error = if (isValid) "" else getString(R.string.alert_invalid_email)
    }

    override fun onPasswordValidated(isValid: Boolean) {
        form_password.error = if (isValid) "" else getString(R.string.alert_invalid_password)
    }

    override fun onFormsValidated(isValid: Boolean) {
        bt_login.isEnabled = isValid
    }

    override fun onPresenterAttached() {
        bt_login.setOnClickListener { requestLogin() }

        et_email.addTextChangedListener { presenter.onEmailChanged(it) }
        et_password.addTextChangedListener { presenter.onPasswordChanged(it) }
    }

    override fun onConnectionChanged(isConnected: Boolean) {
    }

    override fun showMainProgressBar(show: Boolean) {
        if (show) {
            hideSoftKeyboard()
        }
        bt_login.isEnabled = !show
        bt_google.isEnabled = !show
    }

    override fun showError(message: String?) {
        message?.let { Snackbar.make(root, it, Snackbar.LENGTH_LONG).show() }
    }

    override fun onLoggedOut(hasLoggedOut: Boolean) {
    }

    private fun requestLogin() {
        presenter.loginUsername(et_email.text.toString(), et_password.text.toString())
    }
}