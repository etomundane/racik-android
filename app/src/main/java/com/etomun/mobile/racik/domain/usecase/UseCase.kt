package com.etomun.mobile.racik.domain.usecase

interface UseCase<RESULT, PARAM> {
    fun execute(param: PARAM): RESULT
}