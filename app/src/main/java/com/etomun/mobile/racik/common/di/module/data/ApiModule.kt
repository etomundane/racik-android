package com.etomun.mobile.racik.common.di.module.data

import com.etomun.mobile.racik.common.di.scope.ApplicationScope
import com.etomun.mobile.racik.data.remote.api.AuthApi
import com.etomun.mobile.racik.data.remote.api.UserApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ApiModule {
    @ApplicationScope
    @Provides
    fun authApi(retrofit: Retrofit): AuthApi {
        return retrofit.create(AuthApi::class.java)
    }

    @ApplicationScope
    @Provides
    fun userApi(retrofit: Retrofit): UserApi {
        return retrofit.create(UserApi::class.java)
    }

}