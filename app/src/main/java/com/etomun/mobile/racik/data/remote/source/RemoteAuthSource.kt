package com.etomun.mobile.racik.data.remote.source

import com.etomun.mobile.racik.data.remote.api.AuthApi
import com.etomun.mobile.racik.data.remote.mapper.BaseResponseMapper
import com.etomun.mobile.racik.data.remote.mapper.LoginResponseMapper
import com.etomun.mobile.racik.domain.entity.param.LoginEmailParam
import com.etomun.mobile.racik.domain.entity.param.SignUpParam
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.entity.result.UserAuth
import com.etomun.mobile.racik.domain.repository.AuthRepository.DataSource
import io.reactivex.Observable
import javax.inject.Inject

class RemoteAuthSource @Inject internal constructor(
    private val authApi: AuthApi,
    private val baseResponseMapper: BaseResponseMapper,
    private val loginResponseMapper: LoginResponseMapper
) : DataSource {
    override fun persistAuth(auth: UserAuth) {
        throw UnsupportedOperationException()
    }

    override fun isLogin(): Boolean {
        throw UnsupportedOperationException()
    }

    override fun loginByEmail(loginEmailParam: LoginEmailParam): Observable<GeneralResult<UserAuth>> {
        return authApi.loginEmail(loginEmailParam.email, loginEmailParam.password)
            .map {
                val userAuth: UserAuth? = if (it.data == null) null else loginResponseMapper.toData(
                    it.data!!
                )
                GeneralResult(
                    it.code, it.error, it.message.toString(), userAuth
                )
            }
    }

    override fun signUp(signUpParam: SignUpParam): Observable<GeneralResult<Boolean>> {
        return authApi.signUp(signUpParam.fullName, signUpParam.email, signUpParam.password)
            .map { baseResponseMapper.toData(it) }
    }

    override fun requestNewPassword(email: String): Observable<GeneralResult<Boolean>> {
        return authApi.requestResetPassword(email)
            .map { baseResponseMapper.toData(it) }
    }

    override fun resetPassword(
        token: String, email: String, password: String
    ): Observable<GeneralResult<Boolean>> {
        return authApi.resetPassword(token, email, password)
            .map { baseResponseMapper.toData(it) }
    }

    override fun logout(): Observable<Boolean> {
        throw UnsupportedOperationException()
    }

    override fun getAuth(): UserAuth {
        throw UnsupportedOperationException()
    }
}