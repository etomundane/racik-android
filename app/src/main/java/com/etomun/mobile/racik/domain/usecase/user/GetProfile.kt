package com.etomun.mobile.racik.domain.usecase.user

import com.etomun.mobile.racik.domain.entity.result.UserProfile
import com.etomun.mobile.racik.domain.repository.UserRepository
import com.etomun.mobile.racik.domain.usecase.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetProfile @Inject internal constructor(
    private val userRepository: UserRepository
) :
    UseCase<Observable<UserProfile>, Void?> {

    override fun execute(param: Void?): Observable<UserProfile> {
        return userRepository.getUserProfile()
    }
}