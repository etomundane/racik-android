package com.etomun.mobile.racik.common.util.helper

import com.etomun.mobile.racik.common.util.state.DayGreetings

interface AppConfig {
    fun getNotificationChannel(channelName: String?, importance: Int): String
    fun getDayGreetings(): DayGreetings
}