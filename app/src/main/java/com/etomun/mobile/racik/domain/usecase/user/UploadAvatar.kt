package com.etomun.mobile.racik.domain.usecase.user

import android.net.Uri
import com.etomun.mobile.racik.domain.entity.result.GeneralResult
import com.etomun.mobile.racik.domain.repository.AuthRepository
import com.etomun.mobile.racik.domain.repository.UserRepository
import com.etomun.mobile.racik.domain.usecase.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class UploadAvatar @Inject internal constructor(
    private val authRepository: AuthRepository,
    private val userRepository: UserRepository
) :
    UseCase<Observable<GeneralResult<Boolean>>, Uri> {

    override fun execute(param: Uri): Observable<GeneralResult<Boolean>> {
        return userRepository.updateAvatar(authRepository.getAuth().id, param)
    }
}